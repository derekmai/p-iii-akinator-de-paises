package testUnitCase;


import java.util.ArrayList;
import implementacionArbolBinario.ArbolBinario;
import implementacionArbolBinario.Main;

import static org.junit.Assert.*;
import org.junit.Test;

public class NodoTest
{
    private ArbolBinario arbol;

    @Test
    public void quedanPreguntasEnAmbasRamasTest()
    {
        // Setup
        arbolBinarioDeCero();

        // Exercise
        boolean compare = Main.quedanPreguntas(arbol.getNodoRaiz());

        // verify

        assertTrue (compare);

    }

    @Test
    public void noQuedanPreguntasTest()
    {
        // Setup
        arbolBinarioSinPreguntas();

        // Exercise

        boolean compare = Main.quedanPreguntas(arbol.getNodoRaiz());

        // verify

        assertFalse (compare);

    }

    @Test
    public void noQuedaPreguntaEnRamaPositivaTest()
    {
        // Setup
        arbolBinarioSinPreguntaRamaPositiva();

        // Exercise

        boolean compare = Main.quedanPreguntas(arbol.getNodoRaiz());

        // verify

        assertTrue (compare);

    }

    @Test
    public void noQuedaPreguntaEnRamaNegativaTest()
    {
        // Setup
        arbolBinarioSinPreguntaRamaNegativa();

        // Exercise
        boolean compare = Main.quedanPreguntas(arbol.getNodoRaiz());

        // verify
        assertTrue (compare);
    }
    @Test
    public void chequearActualizadorConActualizadorCompletoTest()
    {
        // Setup
        arbolBinarioDeCero();
        ArrayList<String>actualizador = new ArrayList<>();
        for (int i = 0; i < 3; i++)
            actualizador.add ("esto no es un test.");

        // Exercise
        arbol.getNodoRaiz().actualizarArbol(actualizador);
    }

    
    @Test(expected = IllegalArgumentException.class)
    public void chequearActualizadorConActualizadorIncompletoTest()
    {
        // Setup
        arbolBinarioDeCero();
        ArrayList<String>actualizador = new ArrayList<>();

        // Exercise
        arbol.getNodoRaiz().actualizarArbol(actualizador);
    }

    @Test
    public void terminarJuegoPorSiTest()
    {
        // Setup
        arbolBinarioDeCero();

        // Exercise
        String resultado = Main.terminarJuego(ArbolBinario.respuestas.SI);

        // Verify
        assertEquals("Te gané!",resultado);
    }

    @Test
    public void terminarJuegoPorNoTest()
    {
        // Setup
        arbolBinarioDeCero();

        // Exercise
        String resultado = Main.terminarJuego(ArbolBinario.respuestas.NO);

        // Verify
        assertEquals("¡Ganaste! ¿En que país Pensabas?",resultado);
    }

    @Test(expected = IllegalArgumentException.class)
    public void mensajeVacioTest()
    {
        // Setup
        arbolBinarioDeCero();

        // Exercise
        arbol.getNodoRaiz().setMensaje("",0);
    }

    
    private void arbolBinarioDeCero()
    {
        arbol = new ArbolBinario("Pregunta","si","no");
    }

    private void arbolBinarioSinPreguntas()
    {
        arbol = new ArbolBinario("Pregunta","si","no");
        arbol.getNodoRaiz().setNodoPositivo(null);
        arbol.getNodoRaiz().setNodoNegativo(null);
    }

    private void arbolBinarioSinPreguntaRamaPositiva()
    {
        arbol = new ArbolBinario("Pregunta","SeríaPreguntaPorSi","SeríaPreguntaPorNo");
        arbol.getNodoRaiz().setNodoPositivo(null);
    }

    private void arbolBinarioSinPreguntaRamaNegativa()
    {
        arbol = new ArbolBinario("Pregunta","SeríaPreguntaPorSi","SeríaPreguntaPorNo");
        arbol.getNodoRaiz().setNodoNegativo(null);
    }

}
