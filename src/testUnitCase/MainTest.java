package testUnitCase;

import static org.junit.Assert.*;
import org.junit.Test;

import implementacionArbolBinario.ArbolBinario;
import implementacionArbolBinario.Main;

public class MainTest
{

    @Test(expected = NullPointerException.class)
    public void chequearIntegridadDeLecturaTest()
    {
    	// Setup
        Main.setArbol (null);
        
        // Exercise
        Main.chequearEstadoArchivoLeido();
    }

    @Test
    public void chequearIntegridadDeLecturaPostCargadoTest()
    { 
    	// Setup
        Main.setArbol (null);
        Main.cargarArbol();
        
        // Exercise
        Main.chequearEstadoArchivoLeido();
    }

    @Test
    public void cargarArbolTest()
    {
    	// Setup
        Main.cargarArbol();

        // Verify
        assertNotNull(Main.getActual());
    }
    

    @Test
    public void ArbolNoLeidoTest()
    {
    	Main.setActual(null);
        assertNull(Main.getActual());
    }

    @Test
    public void chequearNulidadConArbolCargadoTest()
    {
        // Setup
        Main.cargarArbol();

        // Exercise
        Main.chequearNulidadActual();
    }

    @Test(expected = NullPointerException.class)
    public void probarNulidadConArbolCargadoTest()
    {
        // Setup
        Main.cargarArbol();
        Main.setActual(null);

        // Exercise
        Main.chequearNulidadActual();
    }

    @Test(expected = NullPointerException.class)
    public void probarNulidadSinArbolCargadoTest()
    {
        // Setup
        Main.setActual(null);

        // Exercise
        Main.chequearNulidadActual();
    }

    @Test(expected = NullPointerException.class)
    public void iterarConActualNullArbolCargadoTest()
    {
        // Setup
        Main.cargarArbol();
        Main.setActual(null);

        // Exercise
        Main.iterar(ArbolBinario.respuestas.SI);
    }

    @Test(expected = NullPointerException.class)
    public void iterarConActualNullSinArbolCargadoTest()
    {
        // Setup
        Main.setActual(null);

        // Exercise
        Main.iterar(ArbolBinario.respuestas.SI);
    }


    @Test
    public void iterarConArbolCargadoPorSiTest()
    {
        // Setup
        Main.cargarArbol();
        ArbolBinario.Nodo compareTo = Main.getActual().getNodoPositivo();

        // Exercise
        Main.iterar(ArbolBinario.respuestas.SI);

        // Verify
        assertEquals(compareTo, Main.getActual());
    }
    @Test
    public void iterarConArbolCargadoPorNoTest()
    {
        // Setup
        Main.cargarArbol();
        ArbolBinario.Nodo compareTo = Main.getActual().getNodoNegativo();

        // Exercise
        Main.iterar(ArbolBinario.respuestas.NO);

        // Verify
        assertEquals(compareTo, Main.getActual());
    }
    
}
