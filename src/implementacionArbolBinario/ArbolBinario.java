package implementacionArbolBinario;
import java.util.ArrayList;

public class ArbolBinario
{
	public enum respuestas{SI,NO}
	private Nodo nodoRaiz;
	private int cantidadHojas = 0;
    public ArbolBinario(String pregunta, String aciertoPorSi, String aciertoPorNo)
    {
        setNodoRaiz(new Nodo(pregunta));
        getNodoRaiz().setNodoPositivo(new Nodo(aciertoPorSi));
        getNodoRaiz().setNodoNegativo(new Nodo(aciertoPorNo));
    }

    public ArbolBinario()
	{
		setNodoRaiz(null);
	}
    
    public void cantidadNodosHoja(Nodo reco)
    {
    	 if (reco!=null)
    	 {
	    	 if (reco.nodoPositivo==null && reco.nodoNegativo==null)
	    		 setCantidadHojas(getCantidadHojas() + 1);
	    	 cantidadNodosHoja(reco.nodoPositivo);
	    	 cantidadNodosHoja(reco.nodoNegativo);
    	 }
    }
	public Nodo getNodoRaiz()
	{
		return nodoRaiz;
	}

	public void setNodoRaiz(Nodo nodoRaiz)
	{
		this.nodoRaiz = nodoRaiz;
	}

	public int getCantidadHojas()
	{
		return cantidadHojas;
	}

	public void setCantidadHojas(int cantidadHojas) {
		this.cantidadHojas = cantidadHojas;
	}

	public static class Nodo
	{
		private ArrayList<String> mensajes;
		private Nodo nodoNegativo;
		private Nodo nodoPositivo;
		private int iteradorDeMensajes = 0;
		
		Nodo(String mensaje)
		{
		   this.mensajes = new ArrayList<String>();
		   agregarMensaje(mensaje);
		   setNodoNegativo(null);
		   setNodoPositivo(null);
		   
		}
		
		Nodo(ArrayList<String> mensajes)
		{
			this.mensajes = new ArrayList<String>();
			for (String mensaje : mensajes)
				agregarMensaje(mensaje);
		}
		
		public Nodo()
		{
			mensajes = new ArrayList<String>();
			setNodoNegativo(null);
			setNodoPositivo(null);
		}
		
		public void agregarMensaje(String mensaje)
		{
			this.mensajes.add(mensaje);
		}
		
		// Toma la informacion ingresada por el jugador y actualiza el arbol
		public void actualizarArbol(ArrayList<String> updater)
		{
			chequearActualizador(updater);
			//¿Podrías decirme una pregunta para diferenciar la guess del arbol  de lo elegido por el jugador);
			//"Si estuvieses pensando en lo elegido por el jugador, cual sería tu respuesta a esa pregunta?);
			if (updater.get(2).toUpperCase().equals("SI"))
		    {
		    	// Setea el Guess en la rama negativa de la respuesta
		        nodoNegativo = new Nodo(this.getMensajes());
		       // Setea el país pensado por el jugador en la rama positiva
		       nodoPositivo = new Nodo(updater.get(0));
		    }
			
		    else
		    {
		        // Setea el Guess en la rama Positiva de la respuesta
		        nodoPositivo = new Nodo(this.getMensajes());
		        // Setea el país pensado por el jugador en la rama negativa
		        nodoNegativo = new Nodo(updater.get(0));
		    }
		        this.mensajes.clear();
		        agregarMensaje(updater.get(1));
		        updater.clear();
		}
		
		// Codigo Defensivo
		void chequearActualizador(ArrayList<String> updater)
		{
			if (updater.size()<3)
				throw new IllegalArgumentException("se quiso actualizar el arbol con informacion incompleta.");
		}
		
		void chequearMensaje(String nuevoMensaje)
		{
		    if (nuevoMensaje.length()==0)
		    {
		        throw new IllegalArgumentException("Quiso ingresar un mensaje vacío.");
		    }
		}
		
		private void chequearRangoDeIterador()
		{
			if (iteradorDeMensajes < 0 || iteradorDeMensajes>mensajes.size())
				throw new IllegalArgumentException("El valor que quiso ponerle al iterador esta fuera de rango.");
		}
		// Termina CD
		
		// Getters & Setters
		
		public String getMensaje(int i)
		{
		    return mensajes.get(i);
		}
		
		public Nodo getNodoNegativo()
		{
		    return nodoNegativo;
		}
		
		public Nodo getNodoPositivo()
		{
		    return nodoPositivo;
		}
		
		public int getIterador()
		{
			return this.iteradorDeMensajes;
		}
		
		public int getMensajesLength()
		{
			return this.mensajes.size();
		}
		
		public ArrayList<String> getMensajes()
		{
			return this.mensajes;
		}
		
		
		public void setIterador(int i)
		{
			chequearRangoDeIterador();
			this.iteradorDeMensajes = i;
		}
		
		public void setMensaje(String string, int i)
		{
			chequearMensaje(string);
			for(String mensaje : mensajes)
			{
				if (mensajes.indexOf(mensaje) == i)
					mensaje = string;
			}
		}

	  public void setNodoPositivo(Nodo nodo)
	  {
		  nodoPositivo = nodo;
	  }

	  public void setNodoNegativo(Nodo nodo)
	  {
	      nodoNegativo = nodo;
	  }
	    // Termina G&S
	}
}
