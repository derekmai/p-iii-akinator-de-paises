package implementacionArbolBinario;

import java.util.ArrayList;
import desarrolloNuevaSerializacion.Solver;
import implementacionArbolBinario.ArbolBinario.Nodo;
import implementacionArbolBinario.ArbolBinario.respuestas;

public class Main
{
	private static ArbolBinario arbol = null;
	private static ArbolBinario.Nodo actual = null;
	private static ArrayList<String> updater = new ArrayList<>();

	public static String iterar(ArbolBinario.respuestas respuesta)
	{
        chequearNulidadActual();
        if (!quedanPreguntas(actual))
			return terminarJuego(respuesta);
        actual.setIterador(0);
		if (respuesta.equals(ArbolBinario.respuestas.SI))
    	{
			actual = actual.getNodoPositivo();
			if (quedanPreguntas(actual))
				return actual.getMensaje(0);
			else
				return "¿Estabas pensando en "+ actual.getMensaje(0) + " ?";
    	}
    	else
    	{
    		actual = actual.getNodoNegativo();
			if (quedanPreguntas(actual))
				return actual.getMensaje(0);
			else
				return "¿Estabas pensando en "+ actual.getMensaje(0) + " ?";
    	}
	}

    public static void cargarArbol()
	{
    	    arbol = Solver.cargar("src/resources/ArbolFinal.txt");
    	    actual = arbol.getNodoRaiz();
    	    arbol.cantidadNodosHoja(actual);
	}

    private static void guardarArbol()
	{
		Solver.guardar(Main.arbol,"src/resources/ArbolFinal.txt");
	}
    
	public static void guardarInformacionParaActualizar(String informacion)
	{
		getUpdater().add(informacion);
		if (getUpdater().size()>=3)
		{
			actual.actualizarArbol(getUpdater());
			Main.guardarArbol();
		}
	}
	
    public static void jugarDeNuevo()
    {
    	actual = arbol.getNodoRaiz();
    }

    public static String terminarJuego(respuestas respuesta)
    {
    	if (respuesta.equals(ArbolBinario.respuestas.SI))
    		return "¡Te gané!";
    	else
    		return "¡Ganaste! ¿En que país Pensabas?";
    }
    
    public static boolean quedanPreguntas(ArbolBinario.Nodo nodo)
	{
        return (nodo.getNodoNegativo() != null || nodo.getNodoPositivo() != null);
    }
    
    // Codigo Defensivo
    public static void chequearEstadoArchivoLeido()
    {
        if (arbol == null)
            throw new NullPointerException ("El archivo se pudo leer, pero hubo un problema con convertirlo a Arbol.");
    }
    
    public static void chequearNulidadActual()
    {
        if (actual == null)
            throw new NullPointerException("el nodo actual es null.");
    }
    // Termina CD
    
    // Getters y Setters
	public static ArrayList<String> getUpdater()
	{
		return updater;
	}
	public static  Nodo getActual()
	{
		return actual;
	}

	/*
	public static ArbolBinario getarbol()
    {
        return arbol;
    }*/
    public static void setActual(Nodo nuevoActual)
    {
        actual = nuevoActual;
    }

    public static void setArbol(ArbolBinario nuevoArbol)
    {
        arbol = nuevoArbol;
    }
    // Termina G&S

	public static ArbolBinario getArbol()
	{
		return arbol;
	}
}
