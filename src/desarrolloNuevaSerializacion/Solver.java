package desarrolloNuevaSerializacion;

import implementacionArbolBinario.ArbolBinario;
import implementacionArbolBinario.ArbolBinario.Nodo;

import java.io.*;

public class Solver
{
    public static void guardar(ArbolBinario arbol, String source)
    {
        PrintWriter writer = null;
        FileWriter fw = null;
    	 try
         {
             fw = new FileWriter(source);
             writer = new PrintWriter(fw);
         } catch (IOException e)
         {
             e.printStackTrace();
         }
        guardar(arbol.getNodoRaiz(),writer);
        writer.close();
        try
        {
            fw.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private static void guardar (Nodo nodo,PrintWriter writer)
    {
    	writer.println(nodo.getMensajesLength());
    	for (String mensaje : nodo.getMensajes())
    		 writer.println(mensaje);
        if (nodo.getNodoPositivo()!=null)
        {
            guardar(nodo.getNodoPositivo(),writer);
        }
        if (nodo.getNodoNegativo()!=null)
        {
            guardar(nodo.getNodoNegativo(),writer);
        }
        // Estoy ante una respuesta, asi que lo marco y vuelvo para atras
        if (nodo.getNodoNegativo() == null && nodo.getNodoPositivo() == null)
        {
	        writer.println('$');
	        writer.println('$');
        }
        return;
    }
    public static ArbolBinario cargar(String source)
    {
        FileReader fr = null;
        BufferedReader reader = null;
        ArbolBinario arbol = null;
        try
        {
            fr = new FileReader(source);
            reader = new BufferedReader(fr);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        Nodo output = new Nodo();
        cargar(output,reader);
        try
        {
            fr.close();
            reader.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        arbol = new ArbolBinario();
        arbol.setNodoRaiz(output);
        return arbol;
    }
    private static void cargar (Nodo nodo,BufferedReader reader)
    {
        String linea = " ";
        Integer index = 0;
        try
        {
            linea = reader.readLine();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        
        // si queda Texto por pasar
        if (linea!= null)
        {
        	
        	Nodo auxiliar = new Nodo();
        	if(linea.length()==1 && !linea.equals("$"))
        	{
            	index = Integer.parseInt(linea);
            	for (int i = 0; i < index;i++)
            	{
            		try
                    {
                        linea = reader.readLine();
                    } catch (IOException e)
                    {
                        e.printStackTrace();
                    }
            		nodo.agregarMensaje(linea);
            	}
            	nodo.setNodoPositivo(auxiliar);
                cargar(nodo.getNodoPositivo(),reader);
                if(auxiliar.getMensajesLength() == 0)
                	nodo.setNodoPositivo(null);
        	}
        	else if (linea.equals("$"))
        		return;
        	auxiliar = new Nodo(); // ADREDE
            nodo.setNodoNegativo(auxiliar);
            cargar(nodo.getNodoNegativo(),reader);
            if(auxiliar.getMensajesLength() == 0)
            	nodo.setNodoNegativo(null);
        }
        return;
    }
}
