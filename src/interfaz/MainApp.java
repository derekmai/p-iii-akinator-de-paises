package interfaz;

import java.awt.EventQueue;

import javax.swing.*;

import implementacionArbolBinario.ArbolBinario;
import implementacionArbolBinario.Main;
import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class MainApp
{
	private JFrame mainFrame;
	private JPanel panelInicial;
	private JPanel panelJuego;
	private JButton siButton,noButton, noSeButton;
	private JButton agregarButton = null;
	private JLabel preguntaLabel,contadorPaisesRestantes, akinatorLabel;
	private JTextField dataEntryTexto = null;
	private boolean cargarDataConBotones = false;
	private boolean terminar = false;
	private String juegoApuesta;

	public static void main(String[] args)
	{
		EventQueue.invokeLater(() ->
		{
			try
			{
				MainApp window = new MainApp();
				window.mainFrame.setVisible(true);
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		});
	}
	/**
	 * @wbp.parser.entryPoint
	 */
	private MainApp()
	{
		initialize();
	}
	
	private void initialize()
	{
		mainFrame = new JFrame();
		mainFrame.setTitle("Akinator Nacional, Mai Version");
		mainFrame.setBounds(100, 100, 800, 600);
		mainFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		mainFrame.getContentPane().setLayout(null);
		
		panelInicial = new JPanel();
		panelInicial.setName("Inicial");
		panelInicial.setBounds(0, 0, 800, 798);
		mainFrame.getContentPane().add(panelInicial);
		
		JButton jugarButton = new JButton("Jugar");
		jugarButton.setBounds(290, 225, 150, 45);
		jugarButton.addActionListener(e ->
		{
			Main.cargarArbol();
			inicializarPanelJuego();
		});
		panelInicial.setLayout(null);
		panelInicial.add(jugarButton);
		
		JLabel instructivoLabel = new JLabel("Elija un país de cualquier parte del mundo, ¡yo lo adivinaré! (O aprenderé en el intento jeje)");
		instructivoLabel.setHorizontalAlignment(SwingConstants.CENTER);
		instructivoLabel.setBounds(6, 21, 794, 34);
		panelInicial.add(instructivoLabel);
		
		definirAkinatorImagen("src/resources/akinatorInicial.png");
		
	}
	private void definirAkinatorImagen(String imageSource)
	{
		akinatorLabel = new JLabel(new ImageIcon(imageSource));
		akinatorLabel.setBounds(480, 62, 310, 422);
		panelInicial.add(akinatorLabel);
	}
	
	private void actualizarAkinator(String imageSource)
	{
		akinatorLabel.setIcon(new ImageIcon(imageSource));
		akinatorLabel.setBounds(480, 62, 310, 422);
	}
	
	private void inicializarPanelJuego()
	{	
		panelJuego = new JPanel();
		panelJuego.setName("Juego");
		panelJuego.setBounds(0, 0, 800, 598);
		mainFrame.getContentPane().add(panelJuego);
		panelJuego.setLayout(null);
		
		inicializarVisualDeJuego();
		
		panelJuego.setVisible(true);
		panelInicial.setVisible(false);
	}
	
	private void inicializarVisualDeJuego()
	{
		siButton = new JButton("SI");
		siButton.setBounds(290, 240, 150, 45);
		siButton.addActionListener(e ->
		{
			if (terminar)
			{
				Main.jugarDeNuevo();
				terminar =false;
				obtenerNuevoMensaje(getMensaje());
				contadorPaisesRestantes.setVisible(true);
				actualizarContadorDePaises();
				actualizarAkinator("src/resources/akinatorPensando.png");
			}
			else if (cargarDataConBotones)
			{
				cargarDataConBotones = false;
				Main.guardarInformacionParaActualizar("SI");
				preguntarJugarDeNuevo();
			}
			else
			{
				String nuevoMensaje = Main.iterar(ArbolBinario.respuestas.SI);
				actualizarContadorDePaises();
				if (nuevoMensaje.equals("¡Te gané!"))
				{
					actualizarAkinator("src/resources/akinatorGano.png");
					contadorPaisesRestantes.setVisible(false);
					terminar  = true;
					preguntaLabel.setText(nuevoMensaje + " \u00BFQuer\u00E9s jugar de nuevo?");
					panelJuego.repaint();
				}
				else
				{
					manejarPreguntaLabel(nuevoMensaje);
				}
			}

		});
		panelJuego.add(siButton);
		noButton = new JButton("NO");
		noButton.addActionListener(e ->
        {
            if (terminar)
                System.exit(0);

            else if (cargarDataConBotones)
            {
                cargarDataConBotones = false;
                Main.guardarInformacionParaActualizar("NO");
                preguntarJugarDeNuevo();
            }
            else
            {
                String nuevoMensaje = Main.iterar(ArbolBinario.respuestas.NO);
                actualizarContadorDePaises();
                if (nuevoMensaje.equals("¡Ganaste! ¿En que país Pensabas?"))
                {
                	actualizarAkinator("src/resources/akinatorPerdio.png");
                	contadorPaisesRestantes.setVisible(false);
                    juegoApuesta = Main.getActual().getMensaje(0);
                    Main.getActual().setIterador(0);
                    preguntaLabel.setText(nuevoMensaje);
                    inicializarDataEntries();
                    noSeButton.setVisible(false);
                }
                else
	            {
                	manejarPreguntaLabel(nuevoMensaje);
	            }   
            }
        });
		
		noButton.setBounds(290, 330, 150, 45);
		panelJuego.add(noButton);
		
		noSeButton = new JButton("NO SE");
		noSeButton.addActionListener(e ->
        {
        	manejarPreguntaLabel(getMensaje());
        });
		
		noSeButton.setBounds(290, 420, 150, 45);
		panelJuego.add(noSeButton);
		preguntaLabel = new JLabel("");
		obtenerNuevoMensaje(getMensaje());
		preguntaLabel.setHorizontalAlignment(SwingConstants.CENTER);
		preguntaLabel.setBounds(0, 11, 800, 74);
		panelJuego.add(preguntaLabel);
		if (Main.getActual().getMensajesLength()==1)
			noSeButton.setVisible(false);
		
		contadorPaisesRestantes = new JLabel();
		actualizarContadorDePaises();
		contadorPaisesRestantes.setHorizontalAlignment(SwingConstants.CENTER);
		contadorPaisesRestantes.setBounds(180, 110, 400, 20);
		panelJuego.add(contadorPaisesRestantes);
		
		actualizarAkinator("src/resources/akinatorPensando.png");
		panelJuego.add(akinatorLabel);
	}
	
	private void actualizarContadorDePaises()
	{
		Main.getArbol().setCantidadHojas(0);
		Main.getArbol().cantidadNodosHoja(Main.getActual());
		int cantidadDePaises = Main.getArbol().getCantidadHojas();
		if (cantidadDePaises==1)
			contadorPaisesRestantes.setText("¡Estoy pensando en "+Main.getArbol().getCantidadHojas()+" país!");
		else
			contadorPaisesRestantes.setText("¡Estoy pensando en "+Main.getArbol().getCantidadHojas()+" países!");
	}
	
	private void manejarPreguntaLabel(String nuevoMensaje)
	{
		obtenerNuevoMensaje(nuevoMensaje);
		if (Main.getActual().getIterador() < Main.getActual().getMensajesLength())
		{
			noSeButton.setVisible(true);
		}
		else
		{
			noSeButton.setVisible(false);
		}
		panelJuego.repaint();
	}
	private void obtenerNuevoMensaje(String nuevoMensaje)
	{
		preguntaLabel.setText(nuevoMensaje);
		Main.getActual().setIterador(Main.getActual().getIterador()+1);
	}
	
	private String getMensaje()
	{
		return Main.getActual().getMensaje(Main.getActual().getIterador());
	}
	
	private void preguntarJugarDeNuevo()
	{
		terminar  = true;
		dataEntryTexto.setVisible(false);
		preguntaLabel.setText("Gracias, ¡aprendí un poco! \u00BFQuer\u00E9s jugar de nuevo?");
		siButton.setVisible(true);
		noButton.setVisible(true);
		panelJuego.repaint();

	}
	
	
	// Inicializa los componentes Data Entry
	private void inicializarDataEntries()
	{
		if (dataEntryTexto == null && agregarButton == null)
		{
			dataEntryTexto = new JTextField();
			dataEntryTexto.setEnabled(true);
			dataEntryTexto.setBounds(140, 110, 400, 20);
			panelJuego.add(dataEntryTexto);
			dataEntryTexto.setColumns(10);
			
			agregarButton = new JButton("Agregar");
			agregarButton.setBounds(290, 155, 150, 45);
			agregarButton.addActionListener(e ->
            {
                Main.guardarInformacionParaActualizar(dataEntryTexto.getText());
                if(Main.getUpdater().size() == 1)
                    preguntaLabel.setText("¿Podrías decirme una pregunta para diferenciar a "+juegoApuesta + " de "+Main.getUpdater().get(0)+"?");
                else if (Main.getUpdater().size() == 2)
                {
                    cargarDataConBotones = true;
                    dataEntryTexto.setVisible(false);
                    siButton.setEnabled(true);
                    noButton.setEnabled(true);
                    agregarButton.setVisible(false);
                    preguntaLabel.setText("Si estuvieses pensando en "+Main.getUpdater().get(0)+", cual sería tu respuesta a esa pregunta?");
                }
                dataEntryTexto.setText("");
            });
			
			panelJuego.add(agregarButton);
		}
		if (dataEntryTexto != null)
		    dataEntryTexto.setVisible(true);
		agregarButton.setVisible(true);
		siButton.setEnabled(false);
		noButton.setEnabled(false);
		panelJuego.repaint();
	}
}
